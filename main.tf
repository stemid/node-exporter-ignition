data "ignition_file" "container_unit" {
  path = "${var.home}/.config/containers/systemd/node-exporter.container"
  content {
    content = file("${path.module}/templates/node-exporter.container")
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.container_unit.rendered
  ]
}
