output "config" {
  value = data.ignition_config.config
}

# Provide all ignition objects as output so a calling module can put together
# its own ignition_config that results in smaller total size.
output "ignition_user" {
  value = []
}

output "ignition_file" {
  value = [data.ignition_file.container_unit]
}

output "ignition_link" {
  value = []
}

output "ignition_systemd_unit" {
  value = []
}

output "ignition_directory" {
  value = []
}
